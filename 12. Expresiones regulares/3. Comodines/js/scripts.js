/*
    Expresiones regulares:
        Son una secuencia de caracteres que forma un patrón de búsqueda, 
        principalmente utilizada para la búsqueda de patrones de cadenas 
        de caracteres u operaciones de sustituciones.
        https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/
        Regular_Expressions

    Página para evaluar expresiones regulares: https://regex101.com/

    sintaxis:
        / slash de inicio
        patron sintaxis de busqueda
        / slash de fin

        /patron/

    Comodines:
        -Sustitución: Define un comodín dentro del patron. El símbolo es 
        el "." y recibe cualquier caracter como sustitución

        -Listado de caracteres válidos: Entre corchetes se pone una lista 
        de los caracteres válidos. 
        [aeiou] Con esto cogeríamos todas las vocales

        -Rangos: Entre corchetes si ponemos un guión entre dos caracteres 
        establecemos un rango. [a-z] Todas las letras minúsculas.
        
        Los rangos se toman segun el orden de la tabla ASCII
        Tabla ASCII https://ascii.cl/es/

        -Mezcla entre rangos y listas: 
            Podemos unir los dos anteriores en una sola expresión. 
            [0-5ou] Serían números del 0 al 5, la letra "o" y la letra "u"

        -Cadenas completas:
            Para establecer una cadena completa debe ir entre paréntesis, 
            si queremos más palabras irán separadas por un pipe. 
            (lorem|amet) es válida la palabra "lorem" y la palabra "amet"
*/


//Seleccionamos el texto a evaluar
const text = document.getElementById('text').textContent;

//Expresión que valida la palabra foto, con cualquier numero . extension
const regEx = /[(foto)0-9]\.(jpg|gif|png)/; 

//Validamos con la fución o métodos text el texto y devuelve true o false
console.log(regEx.test(text));