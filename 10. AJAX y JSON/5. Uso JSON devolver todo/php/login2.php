<?php
    
    //Almacenamos el valor del input usuario en una variable
	$el_usuario = isset($_GET['usuario']) ? $_GET['usuario'] 
	: $_POST['usuario'];

    //Creamos un array capaz de almacenar un objeto JSON
	$el_array = new stdClass();

    //Evaluamos si el usuario es correcto
	switch($el_usuario) {
		case 'Manuel':
			//Asignamos el valor de cada propiedad de forma clave:valor
			$el_array->Nombre = "Manuel";
			$el_array->Apellido = "Henriquez";
			$el_array->Edad = "26";

	        //Codificamos el objeto JSON que tenemos almacenado en el array
			$json = json_encode($el_array);
			
	        //Devolvemos la respuesta
			echo $json;
		break;

		case 'Raquel':
			//Asignamos el valor de cada propiedad de forma clave:valor
			$el_array->Nombre = "Raquel";
			$el_array->Apellido = "Herrera";
			$el_array->Edad = "23";

	        //Codificamos el objeto JSON que tenemos almacenado en el array
			$json = json_encode($el_array);
			
	        //Devolvemos la respuesta
			echo $json;
		break;

		case 'Ender':
			//Asignamos el valor de cada propiedad de forma clave:valor
			$el_array->Nombre = "Ender";
			$el_array->Apellido = "Paredes";
			$el_array->Edad = "25";

	        //Codificamos el objeto JSON que tenemos almacenado en el array
			$json = json_encode($el_array);
			
	        //Devolvemos la respuesta
			echo $json;
		break;
	}

?>