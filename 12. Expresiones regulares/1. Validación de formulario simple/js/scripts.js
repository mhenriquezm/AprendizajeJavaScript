//Identificar cada elemento y almacenarlo en una constante
const form = document.getElementById('form');
const button = document.getElementById('submitButton');

const name = document.getElementById('name');
const email = document.getElementById('email');
const gender = document.getElementById('gender');
const terms = document.getElementById('terms');

//Crear un objeto con las propiedades válidas
const isValid = {
    name: false,
    email: false,
    gender: false,
    terms: false
};

//Impedimos el envio del formulario
form.addEventListener('submit', (e) => {
    
    //Con una función flecha podemos hacer lo mismo que con una anónima
    e.preventDefault();

    //Llamamos a la función de validar
    validarFormulario();

}, false);

//Validamos los campos
name.addEventListener('change', (e) => {
    
    //Apuntamos al bojeto, su valor, longitud y eliminamos los espacios 
    if(e.target.value.trim().length > 0) {

        isValid.name = true;
    }
    else {
        isValid.name = false;
    }

}, false);

//Change detecta cualquier cambio
email.addEventListener('change', (e) => {
    
    //Apuntamos al bojeto, su valor, longitud y eliminamos los espacios 
    if(e.target.value.trim().length > 0) {

        isValid.email = true;
    }
    else {
        isValid.email = false;
    }

}, false);

gender.addEventListener('change', (e) => {
    
    //Apuntamos al objeto, y si esta checkado
    if(e.target.checked) {

        isValid.gender = true;
    }

}, false);

terms.addEventListener('change', (e) => {

    //Igualamos la propiedad en función el check
    isValid.terms = e.target.checked;

    //En caso de aceptar los términos activamos el botón
    (e.target.checked) ? button.removeAttribute('disabled') 
    : button.setAttribute('disabled', true);

}, false);

//Función para validar el envio
const validarFormulario = () => {

    //Convertimos el objeto en un array para recorrerlo
    const formValues = Object.values(isValid);

    //Comprobamos si existen algun false retorana -1 si no encuentra
    const valid = formValues.findIndex(value => value == false);

    if(valid == -1) {
        form.submit();
    }
    else {
        alert('Campos sin rellenar');
    }

};