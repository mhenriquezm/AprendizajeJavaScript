<?php
    
    //Almacenamos el valor del input usuario en una variable
	$el_usuario = isset($_GET['usuario']) ? $_GET['usuario'] 
	: $_POST['usuario'];

    //Creamos un array capaz de almacenar un objeto JSON
	$el_array = new stdClass();

    //Evaluamos si el usuario es correcto
	if($el_usuario == "Manuel") {

        //Asignamos el valor de cada propiedad de forma clave:valor
		$el_array->Nombre = "Manuel";
		$el_array->Apellido = "Henriquez";
		$el_array->Edad = "26";

        //Codificamos el objeto JSON que tenemos almacenado en el array
		$json = json_encode($el_array);
		
        //Devolvemos la respuesta
		echo $json;

	}

?>