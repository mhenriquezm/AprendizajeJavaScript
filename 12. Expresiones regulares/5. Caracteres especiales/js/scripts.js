/*
    Expresiones regulares:
        Son una secuencia de caracteres que forma un patrón de búsqueda, 
        principalmente utilizada para la búsqueda de patrones de cadenas 
        de caracteres u operaciones de sustituciones.
        https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/
        Regular_Expressions

    Página para evaluar expresiones regulares: https://regex101.com/
    
    Caracteres:
        \s: Coincide con un carácter de espacio, entre ellos incluidos 
        espacio, tab, salto de página, salto de linea y retorno de carro. 
        ^[a-zA-Z]+\s[a-zA-Z]+$
        \S: Coincide con todo menos caracteres de espacio ^\S{5}$
        \d: Coincide con un carácter de número. Equivalente a [0-9] ^\d{5}$
        \D: Coincide con cualquier carácter no numérico. Equivalente a 
        [^0-9] ^\D{5}$
        \w: Coincide con cualquier carácter alfanumérico, incluyendo el 
        guión bajo. Equivalente a [A-Za-z0-9_] ^\w+@$
        \W: Coincide con todo menos caracteres de palabra. ^\W+$
*/


//Seleccionamos el texto a evaluar
const text = document.getElementById('text').textContent;

//Construimos la expresion regular
const regEx = /^\d{4}-\d{7}$/;

//Validamos el texto con la función test
console.log(regEx.test(text));