/*
    Expresiones regulares:
        Son una secuencia de caracteres que forma un patrón de búsqueda, 
        principalmente utilizada para la búsqueda de patrones de cadenas 
        de caracteres u operaciones de sustituciones.
        https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/
        Regular_Expressions

    Página para evaluar expresiones regulares: https://regex101.com/

    sintaxis:
        / slash de inicio
        patron sintaxis de busqueda
        / slash de fin

        /patron/

    Delimitadores:
        ^ Antes de este símbolo no puede haber nada
        $ Después de este símbolo no puede haber nada
        ^hola$
    Cantidad:
        - llaves: lo que está antes tiene que aparecer la cantidad exacta 
        de veces. Hay tres combinaciones posibles. 
            {n} Se tiene que repetir n veces
            {n,m} Se tiene que repetir entre n y m veces, ambas incluidas.
            {n,} Se tiene que repetir como mínimo n veces y sin máximo
        ^[a-zA-Z]{1,3}@{1}$
        -asterisco: Lo que está antes del asterisco puede estar, puede no 
        estar y se puede repetir.  .*@.*\..*
        -interrogación: Lo que está antes de la interrogación puede no 
        estar, pero si está solo puede aparecer una vez.
            ^[ae]?$ 
        - operador +: lo que está antes del + tiene que estár una vez como 
        mínimo
        A-[0-9]+
*/

//Seleccionamos el texto a evaluar
const text = document.getElementById('text').textContent;

//Validación básica de un correo electrónico
const regEx = /.*@.*\..*/;

//Validamos con la fución o métodos text el texto y devuelve true o false
console.log(regEx.test(text));